#!/bin/sh
#
# Docker wrapper for yarn
#

alias yarn="docker run --user $(id -u):$(id -g) --rm -v $(pwd):/app -w /app -it envyarn yarn $@"
alias yarun="docker run --user $(id -u):$(id -g) --rm -v $(pwd):/app -w /app -it envyarn yarn run $@"
alias yit="docker run --user $(id -u):$(id -g) --rm -v $(pwd):/app -w /app -it envyarn yarn init"
alias yarn-update-node-version="docker run --cidfile /tmp/yarn.dockerid envnode apk add yarn && docker commit $(cat /tmp/yarn.dockerid) envyarn && rm /tmp/yarn.dockerid"
