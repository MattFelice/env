#!/bin/sh
#
# Docker wrapper for node
#

alias node="docker run --user $(id -u):$(id -g) --rm -v $(pwd):/app -w /app -it envnode node $@"
alias npm="docker run --user $(id -u):$(id -g) --rm -v $(pwd):/app -w /app envnode npm $@"
alias npx="docker run --user $(id -u):$(id -g) --rm -v $(pwd):/app -w /app envnode npx $@"
alias nit="docker run --user $(id -u):$(id -g) --rm -v $(pwd):/app -w /app -it envnode npm init"
alias node-set-version="docker pull node$@ && docker image tag node$@ envnode"
